import numpy 
import json
import random
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D  
import sys
sys.path.append('../')

from vroute.generator import PathGenerator
from vroute.solver import PathSolver
from vroute.camera import Camera


def generate(fname, camera, show = True):
  gen = PathGenerator()
  path = []
  if len(sys.argv) == 2 and sys.argv[1] == 'linear':
    path = gen.straight(start=(-100, -10), length=40)
  else:
    path = gen.generate()
  path_arr = numpy.array(path)
  cam_path = camera.path2cpath(path)
  image = camera.cpath2camera(cam_path, marker_width=0.75)
  image_arr = numpy.array(image)
  new_path_arr = numpy.array(cam_path)
  x_angular = numpy.degrees(image_arr[:, 0])
  width=numpy.degrees(image_arr[:, 1])	

  with open(fname, 'w') as f:
    data = {}
    data['x'] = list(x_angular)
    data['width'] = list(width)
    data['image'] = [tuple(i) for i in numpy.degrees(image_arr)]
    data['first_marker_xy'] = path[0]
    data['camera_xy'] = camera.camera_loc
    data['camera_view'] = camera.camera_view
    json.dump( data, f)
  
  if not show:
    return

  plt.figure('Generator')

  c = cm.rainbow(numpy.linspace(0, 1, len(path)))
  xs, ys = path_arr[:, 0], path_arr[:, 1]
  ax1 = plt.subplot(221, projection='3d')
  bottom = numpy.array([0 for _ in range(len(path))])
  top = numpy.array([abs(x) + abs(y) + random.randint(20, 50) for x, y in path])
  ax1.bar3d(xs, ys, bottom, 2, 2, top, color=c)
  plt.xlim(-100, 100)
  plt.ylim(-100, 100)

  plt.subplot(222)
  plt.scatter(xs, ys, color = c)
  camera_loc = camera.camera_loc
  camera_view = camera.camera_view
  plt.plot(*camera_loc, 's', color = 'black')
  l =45
  view_line = {'x':[camera_loc[0], camera_loc[0] + l*numpy.cos(numpy.radians(camera_view))], 
               'y': [camera_loc[1], camera_loc[1] + l*numpy.sin(numpy.radians(camera_view))]}

  plt.plot(view_line['x'], view_line['y'], color = 'black')

  plt.subplot(223)
  plt.scatter(new_path_arr[:, 0], new_path_arr[:, 1], color = c)
  plt.plot(0, 0, 's', color = 'black')

  plt.plot([0, l], [0, 0], color = 'black')

  plt.subplot(224) 
  height=numpy.ones_like(width)
  plt.bar(-x_angular, height, width, color=c)
  plt.xlim(-90, 90)

def solve(fname, show=True):
  data = {}
  with open(fname, 'r') as f:
    data = json.load(f)
  camera = Camera(camera_loc = data['camera_xy'], camera_view=data['camera_view'])
  solver = PathSolver(camera)

  path, cam_path = solver.solve(data)

  if not show:
    return

  plt.figure('Solver')

  c = cm.rainbow(numpy.linspace(0, 1, len(path)))
  
  plt.subplot(222)

  path_arr = numpy.array(path)
  xs, ys = path_arr[:, 0], path_arr[:, 1]
  plt.scatter(xs, ys, color = c)
  
  plt.subplot(223)

  cam_path_arr = numpy.array(cam_path)
  xs, ys = cam_path_arr[:, 0], cam_path_arr[:, 1]
  plt.scatter(xs, ys, color = c)

  plt.subplot(224)
  
  height=[1] * len(path)
  plt.bar([-d for d in data['x']], height, data['width'], color=c)
  plt.xlim(-90, 90)



camera = Camera(camera_loc = (0, -45), camera_view = 75)
generate('path.json', camera)
solve('path.json')
plt.show()

