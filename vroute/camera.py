import numpy
from vroute.coordinates import polar2xy
from vroute.coordinates import xy2polar
from vroute.coordinates import rotate_xy
from vroute.coordinates import shift_xy

class Camera:
  def __init__(self, camera_loc=(0, 0), camera_view=0, **kwargs):
    self.camera_loc = camera_loc
    self.camera_view = camera_view
    self.perspective = kwargs.get('perspective', True) # if False orthogonal
    assert self.perspective is True

  def camera2cpath(self, image, marker_width):
    path = []
    for angular_x, angular_w in image:
      d = abs(marker_width / (2 * numpy.tan(numpy.radians(angular_w /2))))
      x, y = polar2xy(d, numpy.radians(angular_x))
      path.append((x, y))
    return path

  def cpath2path(self, cam_path):
    path = []
    camera_loc_opposite = [-c for c in self.camera_loc]
    for x, y in cam_path:
      xn, yn = rotate_xy(x, y, numpy.radians(self.camera_view))
      xn, yn = shift_xy(xn, yn, camera_loc_opposite)
      path.append((xn, yn))
    return path

  def path2cpath(self, path):
    camera_path = []
    xc, yc = self.camera_loc
    for x, y in path:
      xn, yn = shift_xy(x, y, self.camera_loc)
      xn, yn = rotate_xy(xn, yn, -numpy.radians(self.camera_view))
      camera_path.append((xn, yn))
    return camera_path

  def cpath2camera(self, camera_path, marker_width):
    xc, yc = self.camera_loc
    image = []	
    for x, y in camera_path:
      r, phi = xy2polar(x, y)
      angular_w = 2 * numpy.arctan(marker_width/(2*r))
      if self.perspective:
        image.append((phi, angular_w))
      else:
        image.append((-y, angular_w))
    return image

