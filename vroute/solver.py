import numpy
from vroute.coordinates import dist

class PathSolver:

  def __init__(self, camera):
    self.camera = camera

  def solve(self, data, **kwargs):
    assert 'width' in data
    assert 'x' in data
    assert 'image' in data
    assert 'first_marker_xy' in data
    assert 'camera_xy' in data
    assert 'camera_view' in data
    marker_width = self._get_width(data)
    if 'refernce_width' in kwargs:
      assert round(marker_width, 2) == round(kwargs['refernce_width'], 2)
    cam_path = self.camera.camera2cpath(data['image'], marker_width)
    path = self.camera.cpath2path(cam_path)
    return path, cam_path
  
  def _get_width(self, data):
    xc, yc = data['camera_xy']
    x0, y0 = data['first_marker_xy']
    first_dist = dist(xc, yc, x0, y0)
    first_anwidth = data['width'][0]
    marker_width = 2 * first_dist * numpy.tan(numpy.radians(first_anwidth)/2)
    print(marker_width)
    return marker_width
