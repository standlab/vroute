import numpy

def xy2polar(x, y):
  phi = numpy.inf if y == 0  else numpy.arctan(y/x)
  return numpy.sqrt(x**2 + y**2), phi

def polar2xy(r, phi):
  return r * numpy.cos(phi), r * numpy.sin(phi)

def shift_xy(x, y, new_origin=(0, 0)):
  x0, y0 = new_origin
  return x - x0, y - y0

def rotate_xy(x, y, angle):
  xn = x*numpy.cos(angle) - y*numpy.sin(angle) 
  yn = x*numpy.sin(angle) + y*numpy.cos(angle) 
  return xn, yn

def dist(x, y, x0, y0):
  return numpy.sqrt((x - x0)**2 + (y - y0)**2)
