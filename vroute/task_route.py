import math
import random 
import matplotlib.pyplot as plt
import numpy
import matplotlib.cm as cm
import json
from mpl_toolkits.mplot3d import Axes3D  

class Camera:
  def __init__(self, camera_loc=(0, 0), camera_view=0, **kwargs):
    self.camera_loc = camera_loc
    self.camera_view = camera_view
    self.perspective = kwargs.get('perspective', True) # if False orthogonal
    assert self.perspective is True

  def camera2cpath(self, image, marker_width):
    path = []
    for angular_x, angular_w in image:
      d = abs(marker_width / (2 * numpy.tan(numpy.radians(angular_w /2))))
      x, y = polar2xy(d, numpy.radians(angular_x))
      path.append((x, y))
    return path

  def cpath2path(self, cam_path):
    path = []
    camera_loc_opposite = [-c for c in self.camera_loc]
    for x, y in cam_path:
      xn, yn = rotate_xy(x, y, math.radians(self.camera_view))
      xn, yn = shift_xy(xn, yn, camera_loc_opposite)
      path.append((xn, yn))
    return path

  def path2cpath(self, path):
    camera_path = []
    xc, yc = self.camera_loc
    for x, y in path:
      xn, yn = shift_xy(x, y, self.camera_loc)
      xn, yn = rotate_xy(xn, yn, -math.radians(self.camera_view))
      camera_path.append((xn, yn))
    return camera_path

  def cpath2camera(self, camera_path, marker_width):
    xc, yc = self.camera_loc
    image = []	
    for x, y in camera_path:
      r, phi = xy2polar(x, y)
      angular_w = 2 * math.atan(marker_width/(2*r))
      if self.perspective:
        image.append((phi, angular_w))
      else:
        image.append((-y, angular_w))
    return image


class PathGenerator:
  
  def __init__(self, max_curvature=90, edge=5, seed=42):
    self.max_curvature = max_curvature
    self._curv_step = 10
    self.edge = edge
    random.seed(seed)
    self.curvature = random.randint(0, 10)
  
  def generate(self, length=20, start =(0, 0)):
    path = [start]
    for _ in range(length):
      v = random.randint(-1,1)
      self.curvature += v * self._curv_step
      if abs(self.curvature) > self.max_curvature:
        self.curvature -= v * self._curv_step
      x, y = path[-1]
      angle = math.radians(self.curvature)
      xn, yn = x + self.edge*math.cos(angle), y + self.edge*math.sin(angle) 
      path.append((xn, yn))	
    return path

  def straight(self, length=20, start =(0, 0)):
    path = [start]
    for _ in range(length):
      x, y = path[-1]
      angle = math.radians(self.curvature)
      xn, yn = x + self.edge*math.cos(angle), y + self.edge*math.sin(angle) 
      path.append((xn, yn))	
    return path

class PathSolver:

  def __init__(self, camera):
    self.camera = camera

  def solve(self, data, **kwargs):
    assert 'width' in data
    assert 'x' in data
    assert 'image' in data
    assert 'first_marker_xy' in data
    assert 'camera_xy' in data
    assert 'camera_view' in data
    marker_width = self._get_width(data)
    if 'refernce_width' in kwargs:
      assert round(marker_width, 2) == round(kwargs['refernce_width'], 2)
    cam_path = self.camera.camera2cpath(data['image'], marker_width)
    path = self.camera.cpath2path(cam_path)
    return path, cam_path
  
  def _get_width(self, data):
    xc, yc = data['camera_xy']
    x0, y0 = data['first_marker_xy']
    first_dist = dist(xc, yc, x0, y0)
    first_anwidth = data['width'][0]
    marker_width = 2 * first_dist * numpy.tan(numpy.radians(first_anwidth)/2)
    print(marker_width)
    return marker_width


def xy2polar(x, y):
  phi = numpy.inf if y == 0  else numpy.arctan(y/x)
  return numpy.sqrt(x**2 + y**2), phi

def polar2xy(r, phi):
  return r * numpy.cos(phi), r * numpy.sin(phi)

def shift_xy(x, y, new_origin=(0, 0)):
  x0, y0 = new_origin
  return x - x0, y - y0

def rotate_xy(x, y, angle):
  xn = x*numpy.cos(angle) - y*numpy.sin(angle) 
  yn = x*numpy.sin(angle) + y*numpy.cos(angle) 
  return xn, yn


def dist(x, y, x0, y0):
  return numpy.sqrt((x - x0)**2 + (y - y0)**2)


def generate(fname, camera, show = True):
  gen = PathGenerator()
  #path = gen.straight(start=(-100, -10), length=40)
  path = gen.generate()
  path_arr = numpy.array(path)
  cam_path = camera.path2cpath(path)
  image = camera.cpath2camera(cam_path, marker_width=0.75)
  image_arr = numpy.array(image)
  new_path_arr = numpy.array(cam_path)
  x_angular = numpy.degrees(image_arr[:, 0])
  width=numpy.degrees(image_arr[:, 1])	

  with open(fname, 'w') as f:
    data = {}
    data['x'] = list(x_angular)
    data['width'] = list(width)
    data['image'] = [tuple(i) for i in numpy.degrees(image_arr)]
    data['first_marker_xy'] = path[0]
    data['camera_xy'] = camera.camera_loc
    data['camera_view'] = camera.camera_view
    json.dump( data, f)
  
  if not show:
    return

  plt.figure('Generator')

  c = cm.rainbow(numpy.linspace(0, 1, len(path)))
  xs, ys = path_arr[:, 0], path_arr[:, 1]
  max_x, max_y = max(xs), max(ys) 

  ax1 = plt.subplot(221, projection='3d')
  bottom = numpy.array([0 for _ in range(len(path))])
  top = numpy.array([abs(x) + abs(y) + random.randint(20, 50) for x, y in path])
  ax1.bar3d(xs, ys, bottom, 2, 2, top, color=c)
  plt.xlim(-100, 100)
  plt.ylim(-100, 100)

  plt.subplot(222)
  plt.scatter(xs, ys, color = c)
  camera_loc = camera.camera_loc
  camera_view = camera.camera_view
  plt.plot(*camera_loc, 's', color = 'black')
  l =45
  view_line = {'x':[camera_loc[0], camera_loc[0] + l*math.cos(math.radians(camera_view))], 
               'y': [camera_loc[1], camera_loc[1] + l*math.sin(math.radians(camera_view))]}

  plt.plot(view_line['x'], view_line['y'], color = 'black')

  plt.subplot(223)
  plt.scatter(new_path_arr[:, 0], new_path_arr[:, 1], color = c)
  plt.plot(0, 0, 's', color = 'black')

  plt.plot([0, l], [0, 0], color = 'black')

  plt.subplot(224) 
  height=numpy.ones_like(width)
  plt.bar(-x_angular, height, width, color=c)
  plt.xlim(-90, 90)

def solve(fname, show=True):
  data = {}
  with open(fname, 'r') as f:
    data = json.load(f)
  camera = Camera(camera_loc = data['camera_xy'], camera_view=data['camera_view'])
  solver = PathSolver(camera)

  path, cam_path = solver.solve(data)

  if not show:
    return

  plt.figure('Solver')

  c = cm.rainbow(numpy.linspace(0, 1, len(path)))
  
  plt.subplot(222)

  path_arr = numpy.array(path)
  xs, ys = path_arr[:, 0], path_arr[:, 1]
  plt.scatter(xs, ys, color = c)
  
  plt.subplot(223)

  cam_path_arr = numpy.array(cam_path)
  xs, ys = cam_path_arr[:, 0], cam_path_arr[:, 1]
  plt.scatter(xs, ys, color = c)

  plt.subplot(224)
  
  height=[1] * len(path)
  plt.bar([-d for d in data['x']], height, data['width'], color=c)
  plt.xlim(-90, 90)



camera = Camera(camera_loc = (0, -45), camera_view = 75)
generate('path.json', camera)
solve('path.json')
plt.show()

