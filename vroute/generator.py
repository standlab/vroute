import random
import numpy

class PathGenerator:
  
  def __init__(self, max_curvature=90, edge=5, seed=42):
    self.max_curvature = max_curvature
    self._curv_step = 10
    self.edge = edge
    random.seed(seed)
    self.curvature = random.randint(0, 10)
  
  def generate(self, length=20, start =(0, 0)):
    path = [start]
    for _ in range(length):
      v = random.randint(-1,1)
      self.curvature += v * self._curv_step
      if abs(self.curvature) > self.max_curvature:
        self.curvature -= v * self._curv_step
      x, y = path[-1]
      angle = numpy.radians(self.curvature)
      xn, yn = x + self.edge*numpy.cos(angle), y + self.edge*numpy.sin(angle) 
      path.append((xn, yn))	
    return path

  def straight(self, length=20, start =(0, 0)):
    path = [start]
    for _ in range(length):
      x, y = path[-1]
      angle = numpy.radians(self.curvature)
      xn, yn = x + self.edge*numpy.cos(angle), y + self.edge*numpy.sin(angle) 
      path.append((xn, yn))	
    return path