Feel free to clone and use.

Generates path and converts it to camera view
---------------------------------------------

`python test_solver.py`

Will produce:

![curved](https://cloud.iszf.irk.ru/index.php/s/7rchXAmDvYb8kNH/download)

`python test_solver.py linear`

Will produce:

![linear](https://cloud.iszf.irk.ru/index.php/s/uLHcmstNUlEGwG2/download)

The json file is saved for just next to test the PathSolver contains solution 
to for path from camera. You can use json to produce your own algorithm.




